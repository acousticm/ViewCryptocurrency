package com.acousticm.viewcryptocurrency.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Zephy on 3/8/2018.
 */

public class Coin {

    @SerializedName("id")
    @Expose
    public String id;
    @SerializedName("name")
    @Expose
    public String name;
    @SerializedName("symbol")
    @Expose
    public String symbol;
    @SerializedName("rank")
    @Expose
    public String rank;
    @SerializedName("price_usd")
    @Expose
    public String priceUsd;
    @SerializedName("price_btc")
    @Expose
    public String priceBtc;
    @SerializedName("24h_volume_usd")
    @Expose
    public String _24hVolumeUsd;
    @SerializedName("market_cap_usd")
    @Expose
    public String marketCapUsd;
    @SerializedName("available_supply")
    @Expose
    public String availableSupply;
    @SerializedName("total_supply")
    @Expose
    public String totalSupply;
    @SerializedName("max_supply")
    @Expose
    public String maxSupply;
    @SerializedName("percent_change_1h")
    @Expose
    public String percentChange1h;
    @SerializedName("percent_change_24h")
    @Expose
    public String percentChange24h;
    @SerializedName("percent_change_7d")
    @Expose
    public String percentChange7d;
    @SerializedName("last_updated")
    @Expose
    public String lastUpdated;
    @SerializedName("price_thb")
    @Expose
    public Double priceThb;
    @SerializedName("24h_volume_thb")
    @Expose
    public String _24hVolumeThb;
    @SerializedName("market_cap_thb")
    @Expose
    public String marketCapThb;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPriceUsd() {
        return priceUsd;
    }

    public void setPriceUsd(String priceUsd) {
        this.priceUsd = priceUsd;
    }

    public String getPriceBtc() {
        return priceBtc;
    }

    public void setPriceBtc(String priceBtc) {
        this.priceBtc = priceBtc;
    }

    public String get_24hVolumeUsd() {
        return _24hVolumeUsd;
    }

    public void set_24hVolumeUsd(String _24hVolumeUsd) {
        this._24hVolumeUsd = _24hVolumeUsd;
    }

    public String getMarketCapUsd() {
        return marketCapUsd;
    }

    public void setMarketCapUsd(String marketCapUsd) {
        this.marketCapUsd = marketCapUsd;
    }

    public String getAvailableSupply() {
        return availableSupply;
    }

    public void setAvailableSupply(String availableSupply) {
        this.availableSupply = availableSupply;
    }

    public String getTotalSupply() {
        return totalSupply;
    }

    public void setTotalSupply(String totalSupply) {
        this.totalSupply = totalSupply;
    }

    public String getMaxSupply() {
        return maxSupply;
    }

    public void setMaxSupply(String maxSupply) {
        this.maxSupply = maxSupply;
    }

    public String getPercentChange1h() {
        return percentChange1h;
    }

    public void setPercentChange1h(String percentChange1h) {
        this.percentChange1h = percentChange1h;
    }

    public String getPercentChange24h() {
        return percentChange24h;
    }

    public void setPercentChange24h(String percentChange24h) {
        this.percentChange24h = percentChange24h;
    }

    public String getPercentChange7d() {
        return percentChange7d;
    }

    public void setPercentChange7d(String percentChange7d) {
        this.percentChange7d = percentChange7d;
    }

    public String getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Double getPriceThb() {
        return priceThb;
    }

    public void setPriceThb(Double priceThb) {
        this.priceThb = priceThb;
    }

    public String get_24hVolumeThb() {
        return _24hVolumeThb;
    }

    public void set_24hVolumeThb(String _24hVolumeThb) {
        this._24hVolumeThb = _24hVolumeThb;
    }

    public String getMarketCapThb() {
        return marketCapThb;
    }

    public void setMarketCapThb(String marketCapThb) {
        this.marketCapThb = marketCapThb;
    }
}

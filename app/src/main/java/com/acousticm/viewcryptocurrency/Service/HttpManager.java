package com.acousticm.viewcryptocurrency.Service;

import com.acousticm.viewcryptocurrency.model.Coin;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

/**
 * Created by Zephy on 3/8/2018.
 */

public class HttpManager {
    private static HttpManager instance;

    public static HttpManager getInstance()
    {
        if (instance == null)
            instance = new HttpManager();
        return instance;
    }
    private GotService service;
    private static String BASE_HTTP = "https://api.coinmarketcap.com/v1/ticker/";

    private HttpManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_HTTP)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(GotService.class);
    }

    public GotService getService(){
        return service;
    }

    public interface GotService {
        @GET("?convert=THB&limit=0")
        Call<List<Coin>> getData();
    }
}

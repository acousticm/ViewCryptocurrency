package com.acousticm.viewcryptocurrency.presentation.ShowData;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.acousticm.viewcryptocurrency.R;
import com.acousticm.viewcryptocurrency.Service.HttpManager;
import com.acousticm.viewcryptocurrency.model.Coin;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Zephy on 3/8/2018.
 */

public class ShowDataPresenter implements ShowDataContract.Presenter {

    private onLoadingResult mListener;
    private ShowDataContract.View mView;
    private Context mContext;
    private List<Coin> list = new ArrayList<>();

    public interface onLoadingResult {
        void onLoadingSucccess(String msg);

        void onLoadingFailure(String msg);
    }

    ShowDataPresenter(@NonNull ShowDataContract.View view, Context context, onLoadingResult mListener) {
        this.mView = view;
        this.mContext = context;
        this.mListener = mListener;
        mView.setPresenter(this);
    }

    @Override
    public void onLoadingData() {
        Call<List<Coin>> call = HttpManager.getInstance().getService().getData();
        call.enqueue(new Callback<List<Coin>>() {
            @Override
            public void onResponse(Call<List<Coin>> call, Response<List<Coin>> response) {
                if (response.isSuccessful()) {
                    mListener.onLoadingSucccess("Success");
                    for (int i = 0; i < response.body().size(); i++) {
                        // Initial Data
                        Coin coin = new Coin();
                        coin.setSymbol(response.body().get(i).symbol);
                        coin.setName(response.body().get(i).name);
                        coin.setRank(response.body().get(i).rank);
                        coin.setPriceThb(response.body().get(i).priceThb);
                        coin.setPriceBtc(response.body().get(i).priceBtc);
                        coin.setPercentChange1h(response.body().get(i).percentChange1h);
                        coin.setPercentChange24h(response.body().get(i).percentChange24h);
                        coin.setPercentChange7d(response.body().get(i).percentChange7d);
                        list.add(coin);
                    }
                }else {

                }
            }

            @Override
            public void onFailure(Call<List<Coin>> call, Throwable t) {
                Log.d("Failure ", "Data" + t.toString());
                mListener.onLoadingFailure(t.toString());
            }
        });
    }

    @Override
    public void onLoadData(RecyclerView recyclerView) {
        ShowDataRecyclerAdapter mAdapter = new ShowDataRecyclerAdapter(this);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(mContext);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void holdtoRefresh(final SwipeRefreshLayout swipeRefreshLayout) {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                onLoadingData();
                if(swipeRefreshLayout.isRefreshing()){
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }


    public void onBindShowDataRowViewAtPosition(int position, ShowDataContract.ShowDataRowView rowView) {
        Coin repo = list.get(position);
        String imgURL = "https://res.cloudinary.com/dxi90ksom/image/upload/";
        rowView.setCurrencyName(repo.getRank() +". "+ repo.getSymbol() + " | " + repo.getName());
        rowView.setCurrency(repo.getPriceThb());
        rowView.setpercenthour("1 ชั่วโมง " + repo.getPercentChange1h() + "%");
        rowView.setpercenttwentyfourhour("24 ชั่วโมง " + repo.getPercentChange24h() + "%");
        rowView.setpercentsevenday("7วัน " + repo.getPercentChange7d() + "%");
        rowView.setImageIco(imgURL + repo.getSymbol().toLowerCase() + ".PNG");
    }

    public int getItemCount() {
        return list.size();
    }

    @Override
    public void start() {

    }
}

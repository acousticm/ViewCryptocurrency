package com.acousticm.viewcryptocurrency.presentation.ShowData;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import com.acousticm.viewcryptocurrency.R;
import com.acousticm.viewcryptocurrency.bases.BaseActivity;

import static android.support.v4.util.Preconditions.checkNotNull;

public class ShowDataActivity extends BaseActivity implements ShowDataContract.View, ShowDataPresenter.onLoadingResult {

    ShowDataContract.Presenter mPresenter;
    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        initInstances();
    }

    public void initInstances() {
        //
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter = new ShowDataPresenter(this, this, this);
        mPresenter.onLoadingData();
        mPresenter.holdtoRefresh(swipeRefreshLayout);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setPresenter(ShowDataContract.Presenter presenter) {
        mPresenter = checkNotNull(presenter);
    }

    @Override
    public void onGetSuccess(String msg) {
        showMsgSuccess(msg);
    }

    @Override
    public void onGetFailure(String msg) {
        showMsgFail(msg);
    }

    @Override
    public void onLoadingSucccess(String msg) {
        showMsgSuccess(msg);
        mPresenter.onLoadData(recyclerView);
    }

    @Override
    public void onLoadingFailure(String msg) {
        showMsgFail(msg);
    }
}

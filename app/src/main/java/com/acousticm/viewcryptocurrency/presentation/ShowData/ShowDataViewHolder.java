package com.acousticm.viewcryptocurrency.presentation.ShowData;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.acousticm.viewcryptocurrency.R;
import com.squareup.picasso.Picasso;

/**
 * Created by Zephy on 3/8/2018.
 */

public class ShowDataViewHolder extends RecyclerView.ViewHolder implements ShowDataContract.ShowDataRowView {
    private TextView tvCurrency, tvCurrencyName, tvpercentonehour, tvpercenttwentyfour, tvpercentsevenday;
    private ImageView icocurrency;

    public ShowDataViewHolder(View itemView){
        super(itemView);
        tvCurrency = itemView.findViewById(R.id.tvCurrency);
        tvCurrencyName = itemView.findViewById(R.id.tvCurrencyName);
        tvpercentonehour = itemView.findViewById(R.id.tvpercentonehour);
        tvpercenttwentyfour = itemView.findViewById(R.id.tvpercenttwentyfour);
        tvpercentsevenday = itemView.findViewById(R.id.tvpercentsevenday);
        icocurrency = itemView.findViewById(R.id.icocurrency);
    }

    @Override
    public void setCurrencyName(String text) {
        tvCurrencyName.setText(text);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void setCurrency(Double price) {
        tvCurrency.setText(String.format("%.4f", price));
    }

    @Override
    public void setpercenthour(String percenthour) {
        tvpercentonehour.setText(percenthour);
    }

    @Override
    public void setpercenttwentyfourhour(String percenttwentyfourhr) {
        tvpercenttwentyfour.setText(percenttwentyfourhr);
    }

    @Override
    public void setpercentsevenday(String percentsevenday) {
        tvpercentsevenday.setText(percentsevenday);
    }

    @Override
    public void setImageIco(String url) {
        Picasso.get()
                .load(url)
                .into(icocurrency);
    }
}

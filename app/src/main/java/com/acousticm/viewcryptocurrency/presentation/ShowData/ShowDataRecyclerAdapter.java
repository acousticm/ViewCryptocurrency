package com.acousticm.viewcryptocurrency.presentation.ShowData;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.acousticm.viewcryptocurrency.R;


/**
 * Created by Zephy on 3/8/2018.
 */

public class ShowDataRecyclerAdapter extends RecyclerView.Adapter<ShowDataViewHolder> {
    private final ShowDataPresenter presenter;

    public ShowDataRecyclerAdapter(ShowDataPresenter showDataPresenter){
        this.presenter = showDataPresenter;
    }

    @NonNull
    @Override
    public ShowDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ShowDataViewHolder(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.list_coin_detail, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ShowDataViewHolder holder, int position) {
        presenter.onBindShowDataRowViewAtPosition(position, holder);
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}

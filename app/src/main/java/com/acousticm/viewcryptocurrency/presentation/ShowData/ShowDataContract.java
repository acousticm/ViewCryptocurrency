package com.acousticm.viewcryptocurrency.presentation.ShowData;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;

import com.acousticm.viewcryptocurrency.bases.BasePresenter;
import com.acousticm.viewcryptocurrency.bases.BaseView;

/**
 * Created by Zephy on 3/8/2018.
 */

public class ShowDataContract {
    interface View extends BaseView<Presenter> {
        void onGetSuccess(String msg);
        void onGetFailure(String msg);
    }
    interface Presenter extends BasePresenter{
        void onLoadingData();
        void onLoadData(RecyclerView recyclerView);
        void holdtoRefresh(SwipeRefreshLayout swipeRefreshLayout);
    }
    interface ShowDataRowView{
        void setCurrencyName(String text);
        void setCurrency(Double price);
        void setpercenthour(String percenthour);
        void setpercenttwentyfourhour(String percenttwentyfourhr);
        void setpercentsevenday(String percentsevenday);
        void setImageIco(String url);
    }
}

package com.acousticm.viewcryptocurrency.bases;

/**
 * Created by Zephy on 3/8/2018.
 */

public interface BaseView<T> {
    void setPresenter(T presenter);
}

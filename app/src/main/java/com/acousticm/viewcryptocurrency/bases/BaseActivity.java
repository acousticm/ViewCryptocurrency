package com.acousticm.viewcryptocurrency.bases;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.shashank.sony.fancytoastlib.FancyToast;

/**
 * Created by Zephy on 3/8/2018.
 */

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public void showMsgSuccess(String message) {
        FancyToast.makeText(getApplicationContext(),
                message,
                FancyToast.LENGTH_SHORT,
                FancyToast.SUCCESS,
                true)
                .show();
    }

    public void showMsgFail(String message){
        FancyToast.makeText(getApplicationContext(),
                message,
                FancyToast.LENGTH_SHORT,
                FancyToast.ERROR,
                true)
                .show();
    }

    public void showMsgInfo(String message){
        FancyToast.makeText(getApplicationContext(),
                message,
                FancyToast.LENGTH_SHORT,
                FancyToast.INFO,
                true)
                .show();
    }

    public void showMsgWarnning(String message){
        FancyToast.makeText(getApplicationContext(),
                message,
                FancyToast.LENGTH_SHORT,
                FancyToast.WARNING,
                true)
                .show();
    }

    interface Toast {
        void showMsgWarnning();
    }

}
